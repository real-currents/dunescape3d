package com.realcurrents.dunescape

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.google.inject.*

class Dunescape3d : ApplicationAdapter() {
    private lateinit var batch: SpriteBatch
    private lateinit var img: Texture
    private lateinit var injector: Injector
    private val engine = Engine()

    private fun GameModule(dunescape3d: Dunescape3d): Module {
        // Interface instantiations (object: ...) can be converted to lambdas ...
        return object: Module {
            override fun configure (binder: Binder) {
                binder.bind(SpriteBatch::class.java)?.toInstance(dunescape3d.batch)
            }

            // ... if object only consists of overrides, but this has additional members
//            @Provides @Singleton
//            public fun systems (): List<Class<out EntitySystem>> {
//                return listOf(
//                    TrySystem()::class.java
//                )
//            }
        }
    }

    private fun gameSystem(): EntitySystem {
        // Abstact class instantiations cannot be converted to lambdas
        return object: EntitySystem() {
            override fun update(deltaTime: Float) {
                println(deltaTime)
            }
        }
    }

    override fun create() {
        engine.addSystem(gameSystem())
        batch = SpriteBatch()
        img = Texture("core/assets/badlogic.jpg")
        injector = Guice.createInjector(GameModule(this))
    }

    override fun render() {
        Gdx.gl.glClearColor(1f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        engine.update(Gdx.graphics.deltaTime)
        batch.begin()
        batch.draw(img, 0f, 0f)
        batch.end()
    }

    override fun dispose() {
        batch.dispose()
        img.dispose()
    }
}
