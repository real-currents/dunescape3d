fun main (a: Array<String> = arrayOf("x2571")) {
    a.forEach {
        var i = Integer.valueOf(it.substring(1), 16); val c = i;
        while (i < c + 2048) print("x${Integer.toHexString(i).toUpperCase()}: ${(i++).toChar()} ")
    }
}
