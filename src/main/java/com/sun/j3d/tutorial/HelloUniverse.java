package com.sun.j3d.tutorial;

import java.awt.*;
import java.nio.file.Paths;
import javax.swing.*;
import javax.media.j3d.*;
import javax.vecmath.*;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;

import common.J3DLoader;

/**
 * A Java 3D example that displays a spinning 3D colored cube.
 * <p>
 * Compilation (from src directory):
 * javac com/sun.j3d/tutorial/HelloUniverse.java
 * <p>
 * Execution (from src directory):
 * java com.sun.j3d.tutorial.HelloUniverse
 * <p>
 * If something goes wrong, it means you haven't installed
 * Java 3D (or Java) correctly.
 *
 * @author Andrew Davison, ad@fivedots.coe.psu.ac.th
 * @version June 2007
 */
public class HelloUniverse extends JFrame {
    private static J3DLoader j3DLoader;

    // ------------------------------------------------------------
    public static void main(String args[]) {
        String classPath = System.getProperty("java.class.path");
        String libPath = System.getProperty("java.library.path");

        System.err.println("Class path: "+ classPath);

        System.err.println("Lib path: "+ libPath);
        
        j3DLoader = new J3DLoader();

        GraphicsConfiguration config;
        CgShaderProgram cg;
        Canvas3D c3d;

        try {
            // Get the preferred graphics configuration for the default screen
            config = SimpleUniverse.getPreferredConfiguration();

            // Create a Cg shader
            cg = new CgShaderProgram();

            c3d = new Canvas3D(config);

            new HelloUniverse(config, c3d);

        } catch (Error linkError) {
            System.out.print(linkError.getMessage());

            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            for (int j = 0; j < gs.length; j++) {
                GraphicsDevice gd = gs[j];
                GraphicsConfiguration[] gc = gd.getConfigurations();
                for (int i = 0; i < gc.length; i++) {
                    c3d = new Canvas3D(gc[i]);
                    HelloUniverse app = new HelloUniverse(gs[j].getDefaultConfiguration(), c3d);
                    Rectangle gcBounds = gc[i].getBounds();
                    int xoffs = gcBounds.x;
                    int yoffs = gcBounds.y;
                    app.setLocation((i * 50) + xoffs, (i * 60) + yoffs);
                }
            }
        }
    }
    // ------------------------------------------------------------

    public HelloUniverse(GraphicsConfiguration config, Canvas3D c3d) {
        super(config);

        // create a Swing panel inside the JFrame
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.setPreferredSize(new Dimension(800, 600));
        this.getContentPane().add(p, BorderLayout.CENTER);

        // add the 3D canvas to panel
        p.add(this.createCanvas3D(config, c3d), BorderLayout.CENTER);

        // configure the window (JFrame)
        this.setTitle("HelloUniverse");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }  // end of HelloUniverse()


    private Canvas3D createCanvas3D(GraphicsConfiguration config, Canvas3D c3d) {
  /* Build a 3D canvas holding a SimpleUniverse which contains
     our 3D scene (a rotating colored cube) */

        // Add a VirtualUniverse to Canvas
        UniverseBuilder univ = new UniverseBuilder(c3d);

        // Create a simple universe
        //SimpleUniverse univ = new SimpleUniverse(c3d);

        // Move the camera back a bit so the cube can be seen
        //univ.getViewingPlatform().setNominalViewingTransform();

        // Ensure at least one redraw every 5 ms
        //univ.getViewer().getView().setMinimumFrameCycleTime(25);

        // Add the scene to the universe
        BranchGroup scene = createSceneGraph();
        univ.addBranchGraph(scene);

        return c3d;
    }  // end of createCanvas3D()


    public BranchGroup createSceneGraph() {
  /* The scene graph is:
         scene ---> tg ---> colored cube
               |
               ---> rotator
  */
        BranchGroup scene = new BranchGroup();

    /* Create a TransformGroup node. Enable its TRANSFORM_WRITE 
       capability so it can be affected at run time */
        TransformGroup tg = new TransformGroup();
        tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        scene.addChild(tg);   // add to the scene

        // connect a coloured cube to the TransformGroup
        tg.addChild(new ColorCube(0.4));

    /* Create a rotation behaviour (a rotation interpolator) which will
       make the cube spin around its y-axis, taking 4 secs to do one
       rotation. */
        Transform3D yAxis = new Transform3D();
        Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE, 0, 0, 4000, 0, 0, 0, 0, 0);
        RotationInterpolator rotator = new RotationInterpolator(rotationAlpha, tg, yAxis, 0.0f, (float) Math.PI * 2.0f);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        rotator.setSchedulingBounds(bounds);
        scene.addChild(rotator);    // add to the scene

        // optimize the scene graph
        scene.compile();
        return scene;
    }  // end of createSceneGraph()

} // end of HelloUniverse class

class UniverseBuilder {

    // User-specified canvas
    Canvas3D canvas;

    // Scene graph elements to which the user may want access
    VirtualUniverse universe;
    Locale locale;
    TransformGroup vpTrans;
    View view;

    public UniverseBuilder(Canvas3D c) {
        this.canvas = c;


        // Establish a virtual universe that has a single
        // hi-res Locale
        universe = new VirtualUniverse();
        locale = new Locale(universe);

        // Create a PhysicalBody and PhysicalEnvironment object
        PhysicalBody body = new PhysicalBody();
        PhysicalEnvironment environment = new PhysicalEnvironment();

        // Create a View and attach the Canvas3D and the physical
        // body and environment to the view.
        view = new View();
        view.addCanvas3D(c);
        view.setPhysicalBody(body);
        view.setPhysicalEnvironment(environment);

        // Create a BranchGroup node for the view platform
        BranchGroup vpRoot = new BranchGroup();

        // Create a ViewPlatform object, and its associated
        // TransformGroup object, and attach it to the root of the
        // subgraph. Attach the view to the view platform.
        Transform3D t = new Transform3D();
        t.set(new Vector3f(0.0f, 0.0f, 2.0f));
        ViewPlatform vp = new ViewPlatform();
        vpTrans = new TransformGroup(t);

        vpTrans.addChild(vp);
        vpRoot.addChild(vpTrans);

        view.attachViewPlatform(vp);

        // Attach the branch graph to the universe, via the
        // Locale. The scene graph is now live!
        locale.addBranchGraph(vpRoot);
    }

    public void addBranchGraph(BranchGroup bg) {
        locale.addBranchGraph(bg);
    }

}
